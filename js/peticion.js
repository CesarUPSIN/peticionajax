function preticionAjax() {
    const http = new XMLHttpRequest;
    const url = "https://jsonplaceholder.typicode.com/photos";

    http.onreadystatechange = function() {
        // Validar la respuesta de la petición
        if(this.status == 200 && this.readyState == 4) {
            console.log(this.responseText);

            let res = document.getElementById("respuesta");
            res.innerHTML = "";

            const json = JSON.parse(this.responseText);

            let i = 0;

            for(const datos of json) {
                if(i != 100) {
                    res.innerHTML += "<div class='producto'>"+"<picture><img src="+datos.url+">"+"</picture><h3>"+"<picture><img src="+datos.thumbnailUrl+">"+"</picture><h3>"+datos.albumId+"</h3><span>"+datos.id+"</span><p>"+datos.title+"</p></div>";
                    i++;
                }
            }
        }
    }

    http.open('GET', url, true);
    http.send();
}

document.getElementById('btnPeticion').addEventListener('click', preticionAjax);